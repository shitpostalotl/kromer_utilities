import requests, random, pickle

data = pickle.load(open("data", "rb"))

data.append(data[-1]+random.randint((data[-1]>0)*-20, 15))

post = "KROMERWATCH: The value of KROMER has changed by "+str(data[-1]-data[-2])+"! It is now "+str(data[-1])+"!"
print(post)
requests.post('https://botsin.space/api/v1/statuses', data={'status': post}, headers={'Authorization': 'Bearer '+open("TOKEN", "r").read().split('\n', 1)[0]})

pickle.dump(data, open("data", "wb"))
